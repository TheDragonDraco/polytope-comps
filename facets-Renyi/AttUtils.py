#!sage  -python
#from sage.all import *
import numpy as np


def VtoMat(Vrep):
    Mat = []
    for vertex in Vrep:
        Mat.append(list(vertex))

    return Mat

# Clear last col
def colReduceVec(row):
    return row[: len(row) - 2 ] + [ row[-2] - row[-1]]


def compareFaces(f1,f2,errtol=1e-5):
    lat1 = f1.as_polyhedron().face_lattice()
    lat2 = f2.as_polyhedron().face_lattice()
    print f1.vertices(),f2.vertices()

    if (not lat1.is_isomorphic(lat2)):
        return False

    # They have the same length!
    for l1, l2 in zip(list(lat1),list(lat2)):
        print l1,l2		
        sf1 = l1.ambient_Vrepresentation()
        print 'sf1 =', sf1		

        sf2 = l2.ambient_Vrepresentation()
        print 'sf2 = ',sf2
        return		
        v1= findVolume(sf1)
        v2 = findVolume(sf2)
        print 'v1 = ', v1 , ' v2 =', v2		
        if ( abs(v1 - v2) > errtol):
            return False

    return True	

def getVertCounts(Faces):
    counts = {}

    for f in Faces:
        v = len(f.vertices())						
        try:

            counts[v] = counts[v] + 1
        except:
            counts[v] = 1			

    return counts

def compareFaceLattices(f1,f2):
    lat1 = f1.as_polyhedron().face_lattice()
    lat2 = f2.as_polyhedron().face_lattice()


    return lat1.is_isomorphic(lat2)




def findVolume(P):
    M = np.matrix([list(v) for v in P.vertices()])	

    print 'actual dimension = ', P.dim(), ' embedded in R^', M.shape[1]
    r = P.dim()		
    if (r == M.shape[1]):
        return P.volume() 


    # row reduce 

    print M

    M1 = M[:,0:r ]
    s = sum(M[:, r], axis=1)
    M1[:,-1] = M1 [:,-1][:] - s	

    print M1

    P2 = Polyhedron(vertices = np.array(M1), base_ring=RDF)	
    print P2.dim(),M1.shape[1]
    assert(P2.dim() == M1.shape[1])

    if(P2.dim() == 1):
        return np.linalg.norm(M1[:,0] - M1[:,1])
    else:
        return P2.volume()


