#!/media/ganesh/extern/sage/SageMath/sage -python
from AttScen import AttScen
from AttUtils import *
import unittest
from sage.all import *
from ProbParams import ProbParams

class TestAttScen(unittest.TestCase):

    errtol = 1e-4

    def testPsat(self):
        pass
        #s = 1 , d = given
        knownSatDist = [ 0.05,0.10,0.20,0.25,1.0/3.0,0.5]
        knownPSat = [0, 1e-4, 0.4929, 0.7898,0.9635,0.9995]
        totalSlack = 1.0
        numRob = 15

        for i in range(len(knownSatDist)):

            A = AttScen(ProbParams(totalSlack, numRob, 0, knownSatDist[i] ))	
            assert(abs(A.findSatProb() - knownPSat[i]) <= self.errtol)

    def testPsimpVolume(self):
        pass
        dia = 1
        for totalSlack in xrange(15, 30, 5):
            for numRob in xrange(5, 15,1):
                for satDist in xrange(2,10,2):
                    P = ProbParams(totalSlack,numRob, dia, satDist)
                    A = AttScen(P)

                    A.findPSimp() 
                    v1 = float(A.Psimp.volume())									
                    v2 = float(totalSlack - dia) ** numRob / factorial(numRob)
                    assert(abs(v1 - v2) <= self.errtol)					
                    print v1,v2
    def testPsimpVolume(self):
        pass
        dia = 1
        for totalSlack in xrange(15, 30, 5):
            for numRob in xrange(5, 15,1):
                for satDist in xrange(2,10,2):
                    P = ProbParams(totalSlack,numRob, dia, satDist)
                    A = AttScen(P)

                    A.findPSimp() 
                    v1 = float(A.Psimp.volume())									
                    v2 = float(totalSlack - dia) ** numRob / factorial(numRob)
                    assert(abs(v1 - v2) <= self.errtol)	



    def testCFVolume(self):
        pass
        dia = 1
        for totalSlack in xrange(15, 30, 5):
            for numRob in xrange(5, 15,1):
                for satDist in xrange(2,10,2):
                    P = ProbParams(totalSlack,numRob, dia, satDist)
                    A = AttScen(P)

                    A.findPSimp() 
                    A.findPSimpCf()
                    v1 = float(A.PsimpCf.volume()) / float(A.Psimp.volume())


                    v2 = (float(totalSlack - numRob * dia) / float(totalSlack - dia)) ** numRob
                    #print v1, v2					
                    assert(abs(v1 - v2) <= self.errtol)					
                    #print v1,v2


    def testenVolume(self):
        dia = 1
        for totalSlack in xrange(10, 20):
            for numRob in xrange(9,11):
                for satDist in xrange(2,10):
                    P = ProbParams(totalSlack,numRob, dia, satDist)
                    A = AttScen(P)
                    print 's = ', totalSlack, 'n = ', numRob, 'd = ', satDist, 'dia = ', dia

                    A.findPSimpCf() 
                    A.findPSimpSat()

                    print A.PsimpCf.Hrepresentation()
                    print A.Psimpsat.Hrepresentation()
                    #A.findPSimpGen()


                    v1 = float(A.Psimpsat.volume())
                    v2 = float(A.PsimpCf.volume())
                    #assert(v1 <= 1.0)

                    print 'SatVol = ', v1 , 'CfVol = ', v2 
                    try:
                        print '\t pcon = ', v1/v2
                    except:
                        print 'Division by zero,skipping'    
                    #print 'vertices  = ', A.PsimpSat.Vrepresentation()

                    #print 'vertices of Conn = ', A.PsimpConn.Vrepresentation()

                    #v2 = A.findProbSatCfFor()
                    #print v1, v2	





if __name__=='__main__':
    unittest.main()


