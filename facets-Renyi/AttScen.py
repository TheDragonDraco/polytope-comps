#!/media/ganesh/extern/sage/SageMath/sage -python
from math import floor
import scipy.special 
from sage.all import *
from AttUtils import *
import itertools
from pprint import pprint
from ProbParams import ProbParams

class AttScen:
    
    def __init__(self, params):
        self.params = params
        
        self.minRob  = self.params.getMinRobSat()
        self.leftSlack = self.params.getLeftSlack()
    
        # To adjust for case l  = 0
        self.zeroSlacks = self.params.getZeros()
        
    def getPSimpIneqs(self):
        self.PSimpIneqs = []
       
        
        # First ineq : x1 >=0        
        self.PSimpIneqs.append([0] + [1] + [0] * (self.params.dic['numRob'] - 1))     
        for i in xrange(1, self.params.dic['numRob']):
            li = ([0] +  [0] * (i - 1) + [-1, 1] + [0] * (self.params.dic['numRob'] - i - 1))
            self.PSimpIneqs.append(li)
                            


        li = [self.params.getRightEnd()] + [0] * (self.params.dic['numRob'] - 1) + [-1] 
        self.PSimpIneqs.append(li)
            
        
    def getPSimpCfIneqs(self):
        self.getPSimpIneqs()
        self.PSimpCfIneqs = self.PSimpIneqs[:]
        # First ineq : x1 >=0        
        self.PSimpCfIneqs.append([0] + [1] + [0] * (self.params.dic['numRob'] - 1))     
        
        # Intermediate
        for i in xrange(1, self.params.dic['numRob']):
            li = ([-self.params.dic['dia']] +  [0] * (i - 1) + [-1, 1] + [0] * (self.params.dic['numRob'] - i - 1))
            #print 'i = ', i, 'ieq = ', li
            self.PSimpCfIneqs.append(li)
                            


        li = [self.params.getRightEnd()] + [0] * (self.params.dic['numRob'] - 1) + [-1] 
        #print 'final list = ', li
        self.PSimpCfIneqs.append(li) 
        
        
    def getPSimpSatIneqs(self):
        self.getPSimpCfIneqs() 
        self.PSimpSatIneqs = self.PSimpCfIneqs[:]
        self.PSimpSatIneqs.append([self.params.dic['satDist']] + [-1] + [0] * (self.params.dic['numRob'] - 1)) #x1 <= d      
        # Intermediate
                
        for i in xrange(1, self.params.dic['numRob']): 
            li = ([self.params.dic['satDist']] +  [0] * (i - 1) + [+1 ,-1] + [0] * (self.params.dic['numRob'] - i - 1)) #x_{i+1} -x_i <= d 
            self.PSimpSatIneqs.append(li)                            



        li = [-self.params.getRightEnd() + self.params.dic['satDist']] + [0] * (self.params.dic['numRob'] - 1) + [1] 
        self.PSimpSatIneqs.append(li) 
       
               
 

