#!/media/ganesh/extern/sage/SageMath/sage -python

from AttScen import AttScen
from ProbParams import ProbParams
import numpy as np
import os


def generateFile(ineqs, filename):
    with open(filename,'w') as fd:
        print ineqs.shape[0]
        fd.write( str(ineqs.shape[0]) + "\t" + str(ineqs.shape[1]) +"\n")
        rows,cols = ineqs.shape[0], ineqs.shape[1]
        for r in xrange(rows):
            s = str(ineqs.item(r, cols - 1)) + " "
            for  c in xrange(cols-1):
                s += str(-ineqs.item((r,c))) + " "
            s += "\n"
            fd.write(s)    
        
         


dic = {'numRob': 20, 'totalSlack': 100, 'satDist': 5, 'allowColls': False, 'dia': 1}
params = ProbParams(dic)




A = AttScen(params)
A.getPSimpSatIneqs()

arr = np.matrix(A.PSimpSatIneqs)
print(arr)


fname ='ineqs.txt'
generateFile(arr, fname)

os.system('integrate   --monomials=ineqs.txt --valuation=volume  ineqs.txt ')
