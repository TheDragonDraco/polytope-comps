#!/usr/bin/sage -python

from __future__ import division  
from sympy.abc import t,x,y
import sympy
from sage.symbolic.integration.integral import definite_integral
from sage.all import *
from math import sqrt
from sympy.geometry import Circle,Segment,Point,Triangle, intersection
from sympy.solvers import solve
from abc import ABCMeta,abstractmethod
from sage.symbolic.assumptions import assume
from sage.numerical.optimize import find_root

# Boundary Curve
class BCurve:
	__metaclass__ = ABCMeta 
	
	def __init__(self, dic):
		try:
			self.id = dic['id']
			self.name = dic['name']	

			self.x = sympy.symbols('x')
			self.y = sympy.symbols('y')
				
			self.x = dic['x']
			self.y = dic['y']
			self.trange= dic['trange']
			#print self.name, self.x, self.y, self.trange
			assert(self.trange[1] > self.trange[0])
			
			self.isopen = dic['isopen']
			self.curve = sympy.geometry.Curve((self.x,self.y), (t, self.trange[0], self.trange[1]))
			self.s = self.getArcLengthBw(self.trange)
			#print 'Got arc length = ', self.s
		except Exception:
			print 'Incomplete Values'
			raise

	def getPointAtParam(self,tc):
		assert(self.trange[0] <= tc <= self.trange[1])
		return self.curve.subs(t,tc)
	
	@abstractmethod 
	def getPointAtLengthFrac(self,frac):
		return self.getPointAtParam(self.getParamAtLengthFrac(frac))

	@abstractmethod
	def getParamAtLengthFrac(self,frac):
		assert(0 <= frac <= 1.0)


	def getArcLengthBw(self,trange_):
		trange_.sort()
		assert(self.trange[1] >= trange_[1] > trange_[0]  >= self.trange[0])	
		xdot = sympy.diff(self.x,t)
		ydot = sympy.diff(self.y,t)
		#print 'xdot=',xdot, 'ydot', ydot
		self.sdot = sympy.sqrt(xdot ** 2 + ydot ** 2)
		return definite_integral(self.sdot, t, trange_[0], trange_[1])
	
	
	@abstractmethod		
	def doesIntersect(self,entity):
		pass




# Boundary Curve
class LineSegment(BCurve):
		
	def __init__(self, dic):
		super(LineSegment, self).__init__(dic)
		self.segment = Segment(self.curve.subs(t,self.trange[0]), self.curve.subs(t,self.trange[1]))

	def doesIntersect(self,entity):
		return intersection(self.segment,entity) != []

	def getParamAtLengthFrac(self,frac):
		super(LineSegment,self).getPointAtLengthFrac(frac)
		param = (self.trange[1] - self.trange[0]) * frac + self.trange[0]
		return param.evalf()

	
	def getPointAtLengthFrac(self,frac):
		return self.getPointAtParam(self.getParamAtLengthFrac(frac))
	


# This is the curve (t0,t0^2) --> (t1,t1^2)
class Parabola(BCurve):
	def __init__(self,dic):
		assert(dic['y'] == dic['x'] * dic['x'])
		super(Parabola, self).__init__(dic)


	def doesIntersect(self,entity):
		if (isinstance(entity, Point)):
			return abs(entity.y - entity.x **2 ) <= 1e-4

		else:
			raise NotImplementedError

	def getPointAtLengthFrac(self,frac):
		return self.getPointAtParam(self.getParamAtLengthFrac(frac))
	
			  

	def getParamAtLengthFrac(self, frac):
		assert(0 <= frac <= 1.0)
		if (frac <= 1e-5):
			return self.trange[0]
		if ( abs(1.0 - frac) <= 1e-5):
			return self.trange[1]		

		r = var('r')
		assume(r > self.trange[0])
		assume(r < self.trange[1]) 
		L = self.getArcLengthBw([self.trange[0],r])
		#print L
		qe = (L  == frac * self.s) 
		#print 'qe=',qe
		
		rsol = find_root(qe,self.trange[0],self.trange[1])
		return rsol
		
		
		

		  


