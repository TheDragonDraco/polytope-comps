#!/usr/bin/sage -python

import unittest2 as unittest
from BCurve import *
from sympy.abc import t,a
import sympy
from sympy.geometry import Point
from sympy import pi,sin,cos
from Robot import *

class TestCircularRobot(unittest.TestCase):
	def setUp(self):
		t = sympy.symbols('t')
      		self.robdic1 = { 'position': Point(-1,0), 'id':1, 'curve':Circle((0,0),1), 'fov': 5 }
		self.robot1 = CircularRobot(self.robdic1)

		self.robdic2 = { 'position': Point(.5,0), 'id':1, 'curve':Circle((1.5,0),1), 'fov': 0.1}
		self.robot2 = CircularRobot(self.robdic2)

	def tearDown(self):
		 del self.robot1, self.robot2
		 del self.robdic1, self.robdic2

	def testPositionOnCurve(self):
		self.robot1.position = Point(0,1)
		self.failUnlessEqual(self.robot1.isPositionOnCurve(), True)

		self.robot1.position = Point(0,1.5)
		self.failUnlessEqual(self.robot1.isPositionOnCurve(), False)	

		self.robot1.position = Point(0,0.5)
		self.failUnlessEqual(self.robot1.isPositionOnCurve(), False)


	def testDoesCollideWith(self):
		self.failUnlessEqual(self.robot1.doesCollideWith(self.robot2), True)
		self.failUnlessEqual(self.robot2.doesCollideWith(self.robot2), True)
		self.failUnlessEqual(self.robot2.doesCollideWith(self.robot1), True)

		self.robot2.setCurve(Circle(Point(2,0),1))
		self.robot2.position = Point(1,0)
		self.failUnlessEqual(self.robot2.doesCollideWith(self.robot1), False)	

		self.robot2.setCurve(Circle(Point(1.99,0),1))
		self.failUnlessEqual(self.robot2.doesCollideWith(self.robot1), True)	
		
		self.robot2.setCurve(Circle(Point(0,0),1))
		self.failUnlessEqual(self.robot2.doesCollideWith(self.robot1), True)		
				

	def testCanCommunicateWith(self):
		self.robot2.setCurve(Circle(Point(1.5,0),1))
		self.failUnlessEqual(self.robot1.canCommunicateWith(self.robot2), True)
		self.failUnlessEqual(self.robot2.canCommunicateWith(self.robot1), False) 
		self.failUnlessEqual(self.robot1.canCommunicateWith(Point(5,0)), True)
		self.failUnlessEqual(self.robot1.canCommunicateWith(Point(-6,0)), False)
		self.failUnlessEqual(self.robot2.canCommunicateWith(Point(1.4,0)), True)

		self.robot2.fov = 5
		self.failUnlessEqual(self.robot2.canCommunicateWith(self.robot1), True) 


	def testLikeness(self):
		self.robot2.setCurve(Circle(Point(1.5,0),1))
		self.robot2.fov = 5		
		self.failUnlessEqual(self.robot1.isLike(self.robot2), True)
		self.failUnlessEqual(self.robot2.isLike(self.robot1), True)

		self.robot2.fov = 0.1
		self.failUnlessEqual(self.robot2.isLike(self.robot1), False)
		self.failUnlessEqual(self.robot2.hasLikeFov(self.robot1), False)
		self.failUnlessEqual(self.robot2.hasLikeCurve(self.robot1), True)

		self.robot2.curve = Circle(Point(1.5,0),.1)
		self.failUnlessEqual(self.robot2.hasLikeCurve(self.robot1), False)

if __name__ == '__main__':
    unittest.main()	
