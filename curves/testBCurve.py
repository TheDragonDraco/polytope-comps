#!/usr/bin/sage -python

import unittest2 as unittest
from BCurve import *
from sympy.abc import t,a
import sympy
from sympy.geometry import Point
from sympy import pi,sin,cos

class SegmentTest(unittest.TestCase):

    def setUp(self):
	t = sympy.symbols('t')
      	self.curvedic = { 'trange': [1,10], 'x':t, 'y':0, 'name': 'Segment', 'isopen': True, 'id': 1}
	self.curve = LineSegment(self.curvedic)	

    def tearDown(self):
        del self.curvedic
	del self.curve

    def testArcLength(self):
	print 'Testing arclength :', self.curve.s
        self.failUnlessEqual(self.curve.s,9)
		

    def testPointAtParam(self):
	print 'Testing point at param'	
	
	A = Point(1,0)
	B = self.curve.getPointAtParam(1)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)

	A = Point(10,0)
	B = self.curve.getPointAtParam(10)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)	
     	
    def testArcLengthBW(self):
	l = self.curve.getArcLengthBw([1.5,2])
	print 'Testing arclength :', l
        self.failUnlessAlmostEqual(l,0.5,places = 3)

    def testGetPointAtLengthFrac(self):
	l , L = self.curve.getArcLengthBw([1,2]), self.curve.s
	A = self.curve.getPointAtLengthFrac( float(l) / float(L))
	B = Point(2,0)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)			

    def testHavePoint(self):
	self.failUnlessEqual(self.curve.doesIntersect(Point(1,0)), True)
	self.failUnlessEqual(self.curve.doesIntersect(Point(5,0)), True)
     	self.failUnlessEqual(self.curve.doesIntersect(Point(5,0.001)), False)
'''
class CircleTest(unittest.TestCase):

    def setUp(self):
	t = sympy.symbols('t')
      	self.curvedic = { 'trange': [0,2 * pi], 'x':cos(t), 'y':sin(t), 'name': 'Circle', 'isopen': False}
	self.curve = BCurve(self.curvedic)	

    def tearDown(self):
        del self.curvedic
	del self.curve

    def testArcLength(self):
	print 'Testing arclength :', self.curve.s
        self.failUnlessAlmostEqual(self.curve.s, 2 * pi, places = 3)
		

    def testPointAtParam(self):
	print 'Testing point at param'	
	
	A = Point(1,0)
	B = self.curve.getPointAtParam(0)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)

	A = Point(0,1)
	B = self.curve.getPointAtParam(pi / 2)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)		
     	
    def testArcLengthBW(self):
	l = self.curve.getArcLengthBw([0, pi/2])
	print 'Testing arclength :', l
        self.failUnlessAlmostEqual(l, pi / 2,places = 3)
'''


class ParabolaTest(unittest.TestCase):

    def setUp(self):
	t = sympy.symbols('t')
      	self.curvedic = { 'id': 3, 'trange': [0, 5], 'x': t , 'y': t ** 2, 'name': 'Parabola', 'isopen': True}
	self.curve = Parabola(self.curvedic)	

    def tearDown(self):
        del self.curvedic
	del self.curve

    def testArcLength(self):
	print 'Testing arclength :', self.curve.s
        self.failUnlessAlmostEqual(self.curve.s, 25.874, places = 2)
		

    def testPointAtParam(self):
	print 'Testing point at param'	
	
	A = Point(0,0)
	B = self.curve.getPointAtParam(0)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)

	A = Point(3,9)
	B = self.curve.getPointAtParam(3)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)	
     	
    def testArcLengthBW(self):
	l = self.curve.getArcLengthBw([1,2])
	print 'Testing arclength :', l
        self.failUnlessAlmostEqual(l, 3.168,places = 3)

    def testGetPointAtLengthFrac(self):
	l, L = self.curve.getArcLengthBw([2,4]), self.curve.s
	A = self.curve.getPointAtLengthFrac(l/L)
	x = 3.3757136535
	B = Point(x, x**2)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)		
     	
	A = self.curve.getPointAtLengthFrac(0)
	B = Point(0,0)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)
	
	A = self.curve.getPointAtLengthFrac(1)
	B = Point(5,25)
	self.failUnlessAlmostEqual(A.distance(B), 0, places = 3)
			

if __name__ == '__main__':
    unittest.main()
