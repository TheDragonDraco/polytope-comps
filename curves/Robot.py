#!/usr/bin/sage -python

from __future__ import division  
from sympy.abc import t,x,y
import sympy
from sage.all import *
from math import sqrt
from sympy.geometry import Circle,Segment,Point,Triangle, intersection
from sympy.solvers import solve
from abc import ABCMeta,abstractmethod


class Robot:
	__metaclass__ = ABCMeta 
	def __init__(self,dic):
		try:
			self.id = dic['id']			
			self.position = dic['position']
			self.curve = dic['curve']
			self.fov = dic['fov']
			assert(self.isPositionOnCurve())
		
			self.attState = None
		except Exception:
			print 'Incomplete dictionary'

	@abstractmethod
	def setCurve(self,curve):
		pass

	@abstractmethod
	def isPositionOnCurve(self,other):
		pass
	
	@abstractmethod
	def canCommunicateWith(self, other):
		pass


	@abstractmethod
	def doesCollideWith(self,other):
		pass

	@abstractmethod
	def hasLikeCurve(self,other):
		pass

	@abstractmethod
	def hasLikeFov(self,other):
		pass

	def isLike(self,other):
		return self.hasLikeCurve(other) and self.hasLikeFov(other)	
			


class CircularRobot(Robot):
	def __init__(self,dic):
		
		super(CircularRobot, self).__init__(dic)
			
		self.dummy = sympy.geometry.ellipse.Circle( (0,0),1)	
		self.setCurve(dic['curve'])

	def setCurve(self,curve):
		assert(type(curve) == type(self.dummy))
		self.curve = curve
		self.center = self.curve.center
		self.position = self.center.x - self.curve.radius # leftmost point


	def isPositionOnCurve(self):
		I = intersection(self.curve, self.position)
		return len(I)

	def canCommunicateWith(self,other):
		if (isinstance(other, CircularRobot)):
			return self.center.distance(other.center) <= self.fov
		elif (isinstance(other, Point)):
			return self.center.distance(other) <= self.fov
		else:
			raise NotImplementedError('Unknown entity:', other)

	def hasLikeCurve(self,other):
		return self.curve.radius == other.curve.radius

	def hasLikeFov(self, other):
		return self.fov == other.fov


	def doesCollideWith(self,other):
		if (isinstance(other, CircularRobot)):
			I = intersection(self.curve, other.curve)
			print 'Collision intersection = ', I
			if (isinstance(I, Point)) or ( type(I) is list and len(I) < 2):
				return False
			else:
				return True	 
		else:
			raise NotImplementedError('Unknown entity:', other)
	
	

	
		

	
		
