#!/usr/bin/sage -python

from __future__ import division  
from sympy.abc import t,x,y
import sympy
from sage.all import *
from math import sqrt
from sympy.geometry import Circle,Segment,Point,Triangle, intersection
from sympy.solvers import solve
from abc import ABCMeta,abstractmethod
import networkx as nx
from copy import deepcopy


class RobotTeam:
	def __init__(self, dic):	
		try:
			self.id = dic['id']
			self.robots = {}
		except Exception:
			print 'Incomplete dictionary'
		self.distGraph = nx.Graph()

	def addRobot(self,robot):
		self.robots[robot.id] = deepcopy(robot)
		self.distGraph.add_node(robot.id)

	def delRobot(self,robid):
		try:
			self.robots.pop(robid)
			return True
		except Exception:
			return False
	def areAllAlike(self):
		if (len(self.robots) <= 1 ):
			return True
		
		tempRobot = None		
		for robid, robot in self.robots:
			if (tempRobot == None):
				tempRobot = robot
			else:
				if (not tempRobot.isLike(robot)):
					return False

		return True
	

	def doCollide(self):
		allrobots = self.robots.values()	
		l = len(allrobots)
		if (l <= 1):
			return False
		for i in xrange(l - 1):
			for j in xrange(i  + 1, l):
				if (allrobots[i].doesCollideWith(allrobots[j])):
					return True

		return False


	def areAllOfSameAttState(self, attState):
		allrobots = self.robots.values()	
		l = len(allrobots)
		if (l <= 1):
			return False

		for r in allrobots:
			if (r.attState != attState):
				return False
		return True


	def getCollGraph(self):
		allrobots = self.robots.values()	
		self.collGraph = nx.Graph()
		self.collGraph.add_nodes_from(self.robots.keys()) 
		

		l = len(allrobots)
		for i in xrange(l - 1):
			for j in xrange(i+1, l):
				
				if allrobots[i].doesCollideWith(allrobots[j]):
					self.collGraph.add_edge(allrobots[i].id, allrobots[j].id)
					self.collGraph.add_edge(allrobots[j].id, allrobots[i].id)

		return self.collGraph


	def addDistance(self,i,j,dist):
		assert(i in self.robots.keys())
		assert(j in self.robots.keys())

		self.distGraph.add_edge(i,j,weight = dist)
		self.distGraph.add_edge(j,i,weight = dist)


				
	'''Doesn't use LOS Propagation Model''' 
	def getCommGraph(self):
		print 'distGraph = ', self.distGraph.edges()
		allrobots = self.robots.values()	
		self.commGraph = nx.Graph() 
		self.commGraph.add_nodes_from(self.robots.keys())

		l = len(allrobots)
		for i in self.robots.keys():
			for j in self.robots.keys():
				if (i != j):
					#print i, j, 'distance = ', i,j
				
					if self.distGraph[i][j]['weight'] <=  self.robots[i].fov:
						self.commGraph.add_edge(i,j)


		return self.commGraph
		
		
			
	'''Depends on LOS Propagation Model''' 
	def getLosCommGraph(self):
		allrobots = self.robots.values()	
		self.LosCommGraph = nx.Graph() 
		self.LosCommGraph.add_nodes_from(self.robots.keys())

		l = len(allrobots)
		for i in xrange(l):
			for j in xrange(l):
				if (i != j):
					if allrobots[i].canCommunicateWith(allrobots[j]):
						self.LosCommGraph.add_edge(allrobots[i].id, allrobots[j].id)


		return self.LosCommGraph
	

		
								
		
				

	
