#!/usr/bin/sage -python

import unittest2 as unittest
from BCurve import *
from Robot import *
from sympy.abc import t,a
import sympy
from sympy.geometry import Point
from sympy import pi,sin,cos
from scipy.stats import rv_continuous
import numpy as np
from RobotTeam import *
import matplotlib.pyplot as plt
#import plotly as ply


def createLosGraph(posvec,connDist):
	numRob = len(posvec)
	robotTeam = RobotTeam({'id': 1}) 	
	points = [curve.getPointAtLengthFrac(pos / curve.s) for pos in posvec]
	# print 'points =', points	
	i = 1
	for p in points:
		print p.x.n(), p.y.n()
		P = Point(p.x.n(),p.y.n())
		dic = { 'position': P, 'id':i, 'curve':Circle(P,0), 'fov': connDist }
		robot = CircularRobot(dic)
		robotTeam.addRobot(robot)
		i = i + 1



	return robotTeam.getLosCommGraph()
		

def createGraph(curve,posvec,connDist):
	numRob = len(posvec)
	robotTeam = RobotTeam({'id': 1}) 
	params = []
	#print 'numRob = ',numRob
	for i in xrange(numRob):
		P = posvec[i]
		params.append(curve.getParamAtLengthFrac(P / curve.s))
		dic = { 'position': Point(P,0), 'id':i + 1, 'curve':Circle((P,0),0), 'fov': connDist }
		robot = CircularRobot(dic)
		robotTeam.addRobot(robot)


	#print 'Params = ', params
	#print 'Parabola range = ', curve.trange	
	for i in xrange(numRob - 1):
		for j in xrange(i + 1, numRob):
			#print 'Getting Distance between :', i,j
			dist = curve.getArcLengthBw([params[i], params[j]])
			robotTeam.addDistance(i + 1, j + 1, dist)
			#print 'Adding distance:', i, j, ':', dist		


	return robotTeam.getCommGraph()
		
	
	


curvedic = { 'id': 3, 'trange': [-5, 5], 'x': t , 'y': t ** 2, 'name': 'Parabola', 'isopen': True}
curve = Parabola(curvedic)

print 'created parabola of length:', curve.s



distParams = None 
trials = 1000
connDist = curve.s / 5

print 'curve length = ', curve.s, 'conndist = ', connDist
Uni = np.random.uniform(0,curve.s)
numRob = 5

conn1 = 0
conn2 = 0

for i in xrange(trials):
	# Generate Random positions on curve
	posvec = np.random.beta(1,2,numRob) * curve.s
	posvec.sort()
	print posvec
	
	# Create Graph 1: LOS
	G1 = createLosGraph(posvec,connDist)	
	conn1 += nx.is_connected(G1)	
	
	G2 = createGraph(curve,posvec,connDist)
	conn2 += nx.is_connected(G2)	
	print i

	print G1.edges()
	print G2.edges()	
	# Create Graph 2 : Non LOs

print
print 'LOS = ', float(conn1)/ float(trials)
print 'NLOS = ', float(conn2)/ float(trials)


			
