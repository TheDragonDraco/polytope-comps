#!/usr/bin/sage -python

import unittest2 as unittest
from BCurve import *
from sympy.abc import t,a
import sympy
from sympy.geometry import Point
from sympy import pi,sin,cos
from Robot import *
from RobotTeam import *
import networkx as nx 

class RobotTeamTest(unittest.TestCase):

    def setUp(self):
	t = sympy.symbols('t')
	self.robotTeam = RobotTeam({'id': 1}) 
	self.robdic1 = { 'position': Point(-1,0), 'id':1, 'curve':Circle((0,0),1), 'fov': 5 }
	self.robdic2 = { 'position': Point(.5,0), 'id':2, 'curve':Circle((1.5,0),1), 'fov': 0.2}
	self.robdic1c = { 'position': Point(.5,0), 'id':1, 'curve':Circle((1.5,0),1), 'fov': 0.1}

	self.robot1 = CircularRobot(self.robdic1)
	self.robot2 = CircularRobot(self.robdic2)
	self.robot1c = CircularRobot(self.robdic1c)

    def tearDown(self):
        del self.robotTeam	


    def testAddRobots(self):

	self.robotTeam.addRobot(self.robot1)		
	self.robotTeam.addRobot(self.robot1c)
	l = len(self.robotTeam.robots)
	self.failUnlessEqual(l,1)
	
	self.robotTeam.addRobot(self.robot1)
	self.robotTeam.addRobot(self.robot2)
	print 'Robots:', self.robotTeam.robots
	l = len(self.robotTeam.robots)		
	self.failUnlessEqual(l,2)


    def testDelRobots(self):
	
	self.robotTeam.addRobot(self.robot1)
	self.robotTeam.addRobot(self.robot2)
	
	self.robotTeam.delRobot(1)
	self.failUnlessEqual(len(self.robotTeam.robots), 1)

	self.robotTeam.delRobot(5)
	self.failUnlessEqual(len(self.robotTeam.robots), 1)

	self.robotTeam.delRobot(2)
	self.failUnlessEqual(len(self.robotTeam.robots), 0)

    def testCollisions(self):
	self.failUnlessEqual(len(self.robotTeam.robots), 0)
        self.assertFalse(self.robotTeam.doCollide())
		
	G1 = self.robotTeam.getCollGraph()
	G2 = nx.Graph()
	self.assertTrue(nx.is_isomorphic(G1,G2))

	self.robotTeam.addRobot(self.robot1)
        self.assertFalse(self.robotTeam.doCollide())
	
	G1 = self.robotTeam.getCollGraph()
	G2.add_node(1)
	self.assertTrue(nx.is_isomorphic(G1,G2))
	
	self.robotTeam.addRobot(self.robot2)
	self.assertTrue(self.robotTeam.doCollide())
	
	G1 = self.robotTeam.getCollGraph()
	G2.add_node(2)
	G2.add_edge(1,2)
	G2.add_edge(2,1)
	self.assertTrue(nx.is_isomorphic(G1,G2))

    def testLosCommGraph(self):
	self.robotTeam.addRobot(self.robot1)
	self.robotTeam.addRobot(self.robot2)
	G1 =  self.robotTeam.getLosCommGraph()
	
	G2 = nx.Graph()
	G2.add_nodes_from([1,2])
	G2.add_edge(1,2)
	self.assertTrue(nx.is_isomorphic(G1,G2))
		

	


if __name__ == '__main__':
    unittest.main()

		
