#!/usr/bin/python
from intervaltree import IntervalTree
import random
from ProbParams import ProbParams
from Sampler import Sampler
from SamplerFactory import SamplerFactory

class Boundary:	

	def __init__(self,params,distParams,maxtrials):
		self.params = params
		self.sampler = SamplerFactory.create(params,distParams)
		self.itree = IntervalTree()
		self.maxtrials = maxtrials
		self.count = 0

	def addRob(self,key):		
		start = self.sampler.sampleOnce()
		end = start + self.params.dia
		#print start, end
		if (not self.itree.overlaps(start, end)):		
			self.itree.addi(start,end,key)
			self.count += 1
			#print "!"
			
	

	def fill(self):		
		for i in xrange(self.maxtrials):
			self.addRob(i)	

	
	def getTree(self):
		return self.itree
		
