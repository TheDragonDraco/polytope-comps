from scipy.stats import truncnorm
import matplotlib.pyplot as plt
import numpy as np
fig, ax = plt.subplots(1, 1)

a, b = 0.1, 2.0
x = np.linspace(truncnorm.ppf(0.01, a, b, loc = 0.5, scale = 1),truncnorm.ppf(0.99, a, b, loc = 0.5, scale = 1), 100)
ax.plot(x, truncnorm.pdf(x, a, b), 'r-', lw=5, alpha=0.6, label='truncnorm pdf')
plt.show()
