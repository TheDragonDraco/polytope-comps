#!/usr/bin/env python
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import json
import sys
import numpy as np
if __name__ == "__main__":
   try:
		fh = open(sys.argv[1],'r')
   except:
		print 'Unable to read file', fh
		sys.exit() 	


   dic = json.load(fh)

   n, bins, patches = plt.hist(dic['data'], 50, normed=1, facecolor='blue', alpha=0.75)    
   mu = np.mean(dic['data'])
   sigma =  np.var(dic['data'])

   c = dic['distParams']['c']
   print 'c = ', c
   a,b = 0, dic['params']['totalSlack']
   mean = (a + b + c) / 3
   var  = (a**2  + b**2 + c**2 - a*c - b*c - a*b) / 18
   print 'mean =', mean, '\t var=', var    
			  	
   y = mlab.normpdf( bins, mu, sigma)  	
    
     
   plt.savefig(sys.argv[2])	
   plt.show()
