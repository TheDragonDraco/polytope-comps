#!/usr/bin/sage -python
from Boundary import *
import numpy as np
import matplotlib.pyplot as plt
from ProbParams import ProbParams
from Sampler import Sampler
import sys
import os
import json

def getArrays(totalSlack,dia,numRob,trials):
		
	for i in xrange(trials):
		B = Boundary(totalSlack,numRob,dia)
		B.fill()
		starts = [inter[0] for inter in sorted(B.getTree())]
		if (i == 0):
			ostat = np.array(starts)	
		else:
			ostat = np.vstack([ostat, np.array(starts)])
	
	return ostat		




if __name__ == "__main__":
    paramDic = {'totalSlack':200,'dia':1,'satDist':5,'numRob':100,'allowColls':False}
    params = ProbParams(paramDic)
    distParams = {'c':0.5, 'distName':'Tri'}

    trials = 1000
  
    filename = sys.argv[1]
    try:
        os.remove(filename)
    except:
        pass



    dic = {
    'params': paramDic,
     'distParams': distParams, 
	'stopping': 'Fill till space  sat',
    'measured': 'stopping time',
    'method':{'name':'MC', 'trials': trials },
    'data':[],
    } 

    for i in xrange(trials):
        occLength = 0
        B = Boundary(params,distParams, trials)
        B.fill()
        occLength += (B.count * params.dia) 
        dic['data'].append(B.count)

    jsonarray = json.dumps(dic)
    print jsonarray

    try:
        fh = open(filename,'w')
        json.dump(dic,fh)
    except:
        pass

