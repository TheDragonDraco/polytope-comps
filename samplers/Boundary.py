#!/usr/bin/sage -python
import intervaltree
import random
from ProbParams import ProbParams
import numpy as np
import networkx as nx

class Boundary:


	props = 	{	
				'pmon': r'Prob. of Mon.' + r'$p_{mon}$',
				'pcon': r'Prob. of Conn. ' +  r"$p_{con}$", 
				'comps': r"No. of Comps. ${cmp}$",  
				'meandeg': r"Mean Degree $deg$",
				'slen': r'Sensed Length $slen$',
				'psen': r'Prob. of Sens. $p_{sen}$'  	

			    }
	
	def __init__(self, sampler):
		self.sampler = sampler
		self.params = self.sampler.params
		self.stats = {}

		# Static Variables
		
	def clear(self):
	    self.sampler.reset()
	        	
		

	def fill(self):
		self.sampler.reset()		
	    			
		self.tree = intervaltree.IntervalTree()
		count  = 0
		i = 0
		arr  = np.array([])
		while(count  < self.params.numRob):
			start = self.sampler.sampleOnce()
			inter = intervaltree.Interval(start, start + self.params.dia)
			if not self.tree.overlaps(inter):
				self.tree.add(inter)
				arr = np.append(arr,start)
				count += 1
	
		self.sampler.arr = arr
		print ('arr = ', arr)
		self.sampler.findSlacks()
		self.slacks =  self.sampler.slacks
		self.starts = self.sampler.starts
		self.genGraph()	
		


	def isCollFree(self):
		return min(self.slacks) >= self.params.dia		

	def isConn(self):
		if (len(self.slacks) > 2):
			return (max(self.slacks[1:-1]) <= self.params.satDist  and min(self.slacks[1:-1]) >= self.params.dia)
		else:
			return True
		
	def isMon(self):
	    return (self.isConn() and max(self.slacks) <= self.params.satDist)
			
	def isSen(self):
		return max(self.slacks[0], self.slacks[-1]) <= self.params.satDist and max(self.slacks[1:-1]) <= 2* self.params.satDist	

	def genGraph(self):
		arr = self.starts[:]
		arr.sort()
		self.graph = nx.Graph() 
		self.graph.add_nodes_from(arr)


		for i in range(len(arr) - 1):
			for j in range(i + 1, len(arr)):
			
				dist = arr[j] - arr[i]
				if (dist <= self.params.satDist):
					self.graph.add_edge(i,j)
					self.graph.add_edge(j,i)
					
					
		print('Edges = ')
		print(self.graph.edges())			
		

	
	def getSlen(self):
		slen = 0
		for s in self.slacks[1:-1]:
			if (s <= 2*self.params.satDist):
				slen += s
			else:
				slen +=  2 * self.params.satDist
		for s in self.slacks[0], self.slacks[-1]:
			if (s <= self.params.satDist):
				slen += s
			else:
				slen += self.params.satDist
		
			
		return slen

	def getComps(self):
		disConnSlacks = 0
		for s in self.slacks[1:-1]:
			disConnSlacks += (s > self.params.satDist)
		return 1 + disConnSlacks
			
			


	def genStats(self):
		self.stats['pmon'] = int(self.isMon())
		self.stats['pcon'] = int(self.isConn())
		
		self.stats['comps'] = 	self.getComps()
		
		self.genGraph()
		
		deg = list(self.graph.degree().values())
		print('deg = ')
		print(deg)
		self.stats['meandeg'] =  np.mean(deg) * 2 
		print('2* meandeg=' + str(np.mean(deg) * 2))
		
		self.stats['slen'] = self.getSlen()
		self.stats['psen'] = int(self.isSen())
		return self.stats

	
	
	
		
	

		
