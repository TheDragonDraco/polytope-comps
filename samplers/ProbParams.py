#from sage.all import *
from copy import deepcopy

class ProbParams:

    def __init__(self, dic):       
        [self.dic, self.totalSlack, self.numRob, self.dia, self.satDist, self.allowColls] = [deepcopy(dic),dic['totalSlack'], dic['numRob'], dic['dia'], dic['satDist'], dic['allowColls']]

    def getRightEnd(self):
        return self.totalSlack - self.dia

    def getFreeSlack(self):
        return self.totalSlack - (self.numRob + 1) * self.dia * (self.allowColls == False)

    def getFreeCon(self):
        return self.satDist - self.dia * (self.allowColls == False)

    def getMinRobSat(self):     
        return int(self.getFreeSlack() / self.getFreeCon()) 


    def getLeftSlack(self):
        return self.getFreeSlack() - self.getMinRobSat() * self.satDist

    def getZeros(self):
        if (self.dia == 0):
            return self.numRob - self.getMinRobSat()


    def getDic(self):
        return self.dic            










