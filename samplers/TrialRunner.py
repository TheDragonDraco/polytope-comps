from Boundary import Boundary
from ProbParams import ProbParams
from multiprocessing import Pool
import numpy as np

class TrialRunner:
    
    def __init__(self,trials, sampler):
        [self.trials, self.sampler] = [trials, sampler]

        
        
    def runTimes(self,times):

        B = Boundary(self.sampler)
        dic = {key:[] for key in B.props}
        
        
        for _ in range(times):
            B.fill()
            stats =  B.genStats()                
               
            for k, v in stats.items():
                dic[k].append(v)
                
        return dic       
                   
        
    def genStats(self,times):
        dic = self.runTimes(times)
        means = {}
        for k,v in dic.items():
            means[k] = np.mean(v)
            
        return means 
        
            

            

                

        
            

    
    def checkColls(self):
        assert(sum(self.collFree) < self.trials)
        estCollFreeProb = self.cfcounts / float(self.trials)
        return estCollFreeProb

    def checkSat(self):
        #assert(sum(self.collFree) == self.trials)
        #print 'Sat = ', sum(self.sats), 'CollFree=', sum(self.collFree)        
        if (self.cfcounts > 0):
            return float((self.conn))/ float(self.cfcounts)
        else:
            return 'i'


    def printStats(self):
        pass
        #print 'connSlacks = ', float(self.connSlacks) / float(self.cfcounts)
        #print 'edges = ', float(self.edges) / float(self.cfcounts)
        


    

