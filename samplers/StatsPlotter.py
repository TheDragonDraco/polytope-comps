from Boundary import Boundary
#from sage.all import *
import intervaltree

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from ProbParams import ProbParams
from TrialRunner import TrialRunner
import numpy as np
import sys
from Sampler import *
import itertools
from SamplerFactory import *
import seaborn as sns



class StatsPlotter:
	

	def __init__(self,trials, totalSlackRange, diaRange,numRobRange,connDistRange,distName, distParams = None, dirName = ''):
		[self.trials,self.totalSlackRange,self.diaRange,self.numRobRange, self.connDistRange,self.distName, self.distParams, self.dirName] = [trials,totalSlackRange, diaRange,numRobRange,connDistRange,distName,distParams, dirName]

		
	def runFirst(self):
		self.ztabs = {}
		params = ProbParams(self.totalSlackRange[0],self.numRobRange[0],self.diaRange[0],self.connDistRange[0], False)

		self.fileName = 'figs/' + self.dirName + '/' + self.distName + '.txt'
		self.file = open(self.fileName, 'w')

		self.file.write('\nDistname: ' + self.distName + '\tdistParams: ' + str(self.distParams) + '\n')
		

		sampler = SamplerFactory.create(self.distName, params,self.distParams)
		T = TrialRunner(self.trials, sampler)
		T.run()
		for k,v in T.stats.items():
			#print k, ":", "{0:.2f}".format(v), "  "
			#print max(numRob), max(connDist)	
			self.ztabs[k] = np.zeros((max(self.diaRange) + 1, max(self.numRobRange) + 1, max(self.connDistRange) + 1))
			self.ztabs[k][self.diaRange[0]][self.numRobRange[0]][self.connDistRange[0]] = v


	def runOnce(self,params):
		sampler = SamplerFactory.create(self.distName, params, self.distParams)



		T = TrialRunner(self.trials, sampler)
		T.run()
		#print np.mean(T.starts), np.var(T.starts)
		num, bins, patches = plt.hist(T.starts, 50, normed=1, facecolor='green', alpha=0.75)

		#plt.show()	
		for k,v in T.stats.items():
			#print k, ":", "{0:.2f}".format(v), "  "
			if(k in self.ztabs.keys()):
				#print k,n,d
				n = params.numRob
				d = params.satDist
				dia = params.dia
			
				self.ztabs[k][int(dia)][n][d] = v
				#print k,n,d,dia,":", v
			else:
				raise 'Key not found :' + k


	def runAll(self):
		self.runFirst()
		for (s,n,dia,d) in itertools.product(self.totalSlackRange,  self.numRobRange, self.diaRange, self.connDistRange):
			print('Running with s = ', s, '\t n = ', n, '\t dia =', dia ,'\td = ', d)
			params = ProbParams(s,n,dia,d, False)
			self.runOnce(params)
		
		

	def plotAll(self):
		
		count = 1

		for k,v in self.ztabs.items():
			if (k != 'cfcounts'):
				self.file.write( 'Key: ' + k + '\n') 
				#self.file.write('Values size = ', v.shape
				self.file.write(np.array2string(v))
				vred = v[np.ix_(self.diaRange,self.numRobRange,self.connDistRange)]
				self.file.write('\n')		

				propStr = Boundary.props[k]
				print('diaRange= ',self.diaRange)


				fig = plt.figure()
				for i in range(0,len(self.diaRange)):

					print('vred =', vred)
					print('vred.shape= ', vred.shape)
				
					print(propStr)

					prefix = propStr + 'R=' + str(self.diaRange[i])
					title = propStr + r':$R$=' + str(self.diaRange[i])
					
					self.plot(fig, 220 + (i+1) ,prefix, k , self.connDistRange, self.numRobRange, vred[i], ['No. of Robots ' + r'$n$', 'Comm. Range ' +r'$d$', k],title)


				plt.show()
					
		self.file.close()	
				




	def plot(self,fig, num,prefix,key,x,y,ztab, labels,title):

		print('num = ', num)
		ax =  fig.add_subplot(num,projection = '3d')
	
		X, Y = np.meshgrid(x, y)
		Z = np.array(ztab)

		print('shapes:',X.shape, Y.shape, Z.shape)
	

		ax.plot_surface(X, Y, Z)
		print(key, labels)

		ax.set_xlabel(labels[0])
		ax.set_ylabel(labels[1])
		ax.set_zlabel(labels[2])
		ax.set_title(title)

		print('plot name =', prefix + '.png')
		plt.savefig('figs/'  + prefix + key + '.png')
	

		sns.set()
		ax = sns.heatmap(Z)
		sns.plt.show()
		
		


	
