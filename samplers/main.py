#!/usr/bin/sage -python

from multiprocessing import Pool
#from sage.all import *
from Boundary import Boundary
from ProbParams import ProbParams
from TrialRunner import TrialRunner
import numpy as np
import sys
from Sampler import *
import itertools
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import pylab
from StatsPlotter import *

			

	


	


trials = 15000
paramDic = {'totalSlack':200, 'satDist':5,  'dia':1,  'allowColls': False,  'numRob':120}
probParams = ProbParams(paramDic)

sampler = UniformSampler(probParams)
trialRunner = TrialRunner(trials,  sampler)



np.set_printoptions(formatter={'float': lambda x: "{0:0.2f}".format(x)})

print('Params')
for  key,val in paramDic.items():
    print (str(key) + ":" + str(val))

print('Trials '  , trials)

means = trialRunner.genStats(trials)

print(means)






		




		

				
			


