from Sampler import *

class SamplerFactory:

	@staticmethod
	def create(params,distParams = None):
		#print 'distName = ', distName
		distName  = distParams['distName']
		if (distName == 'Uni'):
			return UniformSampler(params)
		elif (distName == 'Histo'):
			return HistoSampler(params,distParams)
		elif (distName == 'Tri'):
			return TriSampler(params,distParams)
		elif (distName == 'Beta'):
			return BetaSampler(params,distParams)
		elif (distName == 'TruncNorm'):
 			return TruncNormSampler(params,distParams)
		elif (distName == 'Poly'):
			return PolySampler(params,distParams)
		elif(distName == 'Mixed'):
			return MixedSampler(params)

		else:
			raise 'Unknown distname'
