import random
from ProbParams import ProbParams
#from sage.all import *
import numpy as np
from abc import ABCMeta, abstractmethod
import scipy.stats
from intervaltree import  *

class Sampler:
	
	def __init__(self, params):
		self.params = params
		self.reset()
		self.distParams = {}
		
	@abstractmethod
	def setDistParams(self):
		pass

	@abstractmethod
	def sample(self):
		pass

	def __str__(self):
		return  str(self.distParams)

	def findSlacks(self):
		# print self.arr
		if (self.params.numRob > 0):
			assert(min(self.arr) >= 0.0)
			assert(max(self.arr) <= self.params.getRightEnd())
		
		self.starts = self.arr
		self.starts.sort()
		print ('sorted starts = ')
		print(self.starts)
		starts2 = np.concatenate([[0],self.starts],axis = 0)
		starts2 = np.concatenate([starts2, [self.params.getRightEnd()]], axis = 0)
		#print 'starts2 =', starts2
		self.slacks =  (np.diff(starts2))



	def reset(self):
		self.starts = [-self.params.dia,  self.params.getRightEnd()]
		self.slacks = []
		self.arr = []

			



class UniformSampler(Sampler):

	def __init__(self, params):
		Sampler.__init__(self, params)
		self.distName = 'Uni'

	def sampleOnce(self):
		return np.random.uniform(0, self.params.getRightEnd())


	def sample(self):
		self.arr = np.random.uniform(0, self.params.getRightEnd(), size=self.params.numRob)



class HistoSampler(Sampler):

	def __init__(self,params,distParams):
		Sampler.__init__(self, params)
		self.distName = 'Histo'
		if (self.checkParams(distParams)):
			self.distParams = distParams
			
	def setDistParams(self):
		if (self.checkParams(distParams)):
			self.distParams = distParams


	def sampleOnce(self):
		r = np.random.uniform(0,1)
		s = 0

		index = 0
	
		bkpoints = np.append([0], self.distParams['bkpoints'])
		probs = np.append([0], self.distParams['probs'])
		cumprobs = np.cumsum(probs)

		print(cumprobs)
		for elem in cumprobs:
			if (r < elem):
				break
			index += 1

		#print 'r=', r
		#print 'index =', index
		#print ' probs = ', probs
			
		t = np.random.uniform(0,1) * (bkpoints[index] - bkpoints[index - 1]) + bkpoints[index - 1]
		assert(t > 0)		
		#print 'random no = ',t
		return t
					
	

	def checkParams(self,distParams):
		self.pieces = len(distParams['probs'])
		
		
	
		assert(len(distParams['bkpoints']) == len(distParams['probs']))
		assert(sorted(distParams['bkpoints']) == distParams['bkpoints'])
		assert(min(distParams['bkpoints']) > 0 and max(distParams['bkpoints']) == self.params.getRightEnd())
		assert(min(distParams['probs']) >= 0 and abs(sum(distParams['probs']) - 1.0) <= 1e-6)		
		
		
		return True 

	
	def sample(self):
		self.arr =  np.zeros(self.params.numRob)
		for i in range(self.params.numRob):
			s = self.sampleOnce()
			self.arr[i] = s		


		
				
class PolySampler(Sampler):
	# f(x) = x^n (n+1) / s^{n+1}
	# F(x) = (x/s) ^{n+1}
	def __init__(self, params, distParams):
		Sampler.__init__(self, params)
		self.distParams = distParams
		self.distName = 'Poly'

	def sampleOnce(self):
		u = np.random.uniform(0, self.params.getRightEnd())
		m = self.distParams['pow']
		return (m+1) * ( u * self.params.getRightEnd()) ** (1.0 / (m + 1)) 	

	def sample(self):
		uni = np.random.uniform(0, self.params.getRightEnd(), size=self.params.numRob)
		m = self.distParams['pow']
		self.arr = [ (m+1) * ( u * self.params.getRightEnd()) ** (1.0 / (m + 1)) for u in uni]

		
	



class BetaSampler(Sampler):

	def __init__(self, params, distParams):
		Sampler.__init__(self, params)
		self.distParams = distParams
		self.distParams['scale'] = self.params.getRightEnd()
		self.distName = 'Beta'	

	def sampleOnce(self):
		return np.random.beta(self.distParams['a'], self.distParams['b'], 1) * self.distParams['scale']


	def sample(self):
		self.arr = np.random.beta(self.distParams['a'], self.distParams['b'], self.params.numRob) * self.distParams['scale']



class TriSampler(Sampler):

	def __init__(self, params, distParams):
		print('Initializing Tri sampler')
		Sampler.__init__(self, params)
		self.distParams = distParams
		self.distParams['loc'] = 0
		self.distParams['scale'] = self.params.getRightEnd()
		self.rv = scipy.stats.triang(distParams['c'], loc = self.distParams['loc'], scale = self.params.getRightEnd())
		self.distName = 'Tri'

	def sampleOnce(self):
		u = self.rv.rvs(1)
		return u[0]


	def sample(self):
		self.arr = self.rv.rvs(self.params.numRob)



class TruncNormSampler(Sampler):
	def __init__(self, params, distParams):
		Sampler.__init__(self, params)
		self.distParams = distParams
		self.rv = scipy.stats.truncnorm(a = 0, b = self.params.getRightEnd())
		self.distName = 'TruncNorm' 

	def sampleOnce(self):
		return self.rv.rvs(1)[0]

	def sample(self):
		self.arr = self.rv.rvs(self.params.numRob)


class MixedSampler(Sampler):
	def __init__(self, params):
		Sampler.__init__(self, params)
		self.distName = 'Mixed' 



	def sample(self):
		size1 = self.params.numRob / 3
		size2 = (self.params.numRob - size1) / 2
		size3 = self.params.numRob - size1 - size2

		

		arr1 = np.random.uniform(0, self.params.getRightEnd(), size = size1)
		
		arr2 = np.random.beta(4, 4, size = size2) * self.params.getRightEnd()

		rv = scipy.stats.truncnorm(a = 0, b = self.params.getRightEnd())
		arr3 = rv.rvs(size3)

		self.arr = np.concatenate((arr1,arr2,arr3))
		


	



