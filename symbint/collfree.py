#!/usr/bin/sage -python

from sage.all import *
from sage.symbolic.integration.integral import definite_integral

def getxvec(n):
	x = list()
	x.append(-R)

	for i in xrange(1,n+1):
		x.append(var ('x_%d' % i))
	return x

def CFIntegral(s,n,R):	
	x = getxvec(n)
	I = 1
	
	for i in xrange(n,0,-1):
		start = x[i-1] + R
		end = s - (n + 1 -i ) * R
		#print i, ":[" ,start , "," , end, "]"	
		I = definite_integral(I, x[i], start, end)
	return I * factorial(n)


def findPsimpVol(s,n,R):
	return (s - R)**n 


def findProb2(s,n,R):
	I = CFIntegral(s,n,R)
	Vol = findPsimpVol(s, n , R)
	return I / Vol


def findProb(s,n,R):
	w = R / float(s - R)
	return (1 - (n - 1)* w ) ** n	
	
s = var('s')
R = var('R')
n = var('n')



for sval in xrange(10,31,1):
	for Rval in xrange(1,  5):
		for nval in xrange(1,sval/Rval):
			p1 = findProb(sval, nval,Rval)
			p2 = findProb2(s, nval, R).subs(s == sval, R == Rval)
		
			print sval, Rval, nval, ":", p1,float(p2)
