#!/usr/bin/python 
import numpy as np
import unittest2 as unittest
from Inter import Inter
from Cuboid import Cuboid
from Hsp import Hsp

class CuboidTest(unittest.TestCase):
	def setUp(self):
		pass
		
	def tearDown(self):
		pass

	def test1(self):
		A = np.array([1,2,3])
		C = Cuboid(A)
		self.failUnlessEqual(C.dim, 3)
		self.failUnlessEqual(C.getVol(), 6)
		print C.getVerts()


class HspTest(unittest.TestCase):
	def setUp(self):
		pass
		
	def tearDown(self):
		pass

	def test1(self):
		A = np.array([1.0,1.0,1.0])
		H = Hsp(A,3)
		self.failUnlessEqual(H.dim, 3)
		print H.inter
		self.failUnlessAlmostEqual(H.getVol(), 27.0/6,3)
		



class InterTest(unittest.TestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def testSimCube1(self):
		A = np.array([1,1])
		C = Cuboid(A)
		
		B = np.array([1.0,1.0])
		H = Hsp(B,1)

		I = Inter(H,C)
		print I.getVol()
		self.failUnlessAlmostEqual(I.getVol(), 1./2.,3)

		
if __name__ == '__main__':
    unittest.main()		
