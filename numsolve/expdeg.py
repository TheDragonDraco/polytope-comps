#!/usr/bin/sage -python

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from scipy.misc import comb
from ProbParams import ProbParams
from Inter import Inter
from scipy.misc import factorial
from math import sqrt

from AttScen import AttScen
from AttUtils import *
from scipy.special import btdtr
from sage.symbolic.integration.integral import definite_integral


def fpar(x,s):
	return 2/(s**2) * x


if __name__ == "__main__":
	s = var('s')
	x1 = var('x1') 
	x2 = var('x2')	
	assume(s>0)
	d = var('d')
	assume(d>0)
	assume(d<s)

	fjoint_u = fpar(x1,s) * fpar(x2,s) 
	D = definite_integral(definite_integral(fjoint_u, x2, x1,s), x1, 0, s)
	print 'D = ', D
	fjoint = fjoint_u / D 


	print fjoint
	I1 = definite_integral(definite_integral(fjoint, x2, x1,x1 + d), x1, 0, s-d)
	I2 = definite_integral(definite_integral(fjoint, x2, x1,s), x1, s-d, s)
	print "I1 = ", I1.full_simplify()
	print "I2 = ", I2.full_simplify()

	I = I1 + I2
	print I.full_simplify()

	D = definite_integral(definite_integral(fjoint, x2, x1,s), x1, 0, s)
	print D.full_simplify()

	print "prob = ", (I).full_simplify()


	Il = I.subs({s:200, d: 5})
	# Dl = D.subs({s:200,d:200})
	print Il.n() 

	Expdeg = 5
	n = Expdeg / (Il.n()) + 1
	print 'n = ', n 
	print 'subs = ', (n-1) * Il.n()

	
	
