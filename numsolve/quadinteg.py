#!/usr/bin/sage -python
from sage.all import *
from sage.symbolic.integration.integral import definite_integral
from operator import mul
from itertools import product
from scipy.misc import comb
import numpy as np

from ProbParams import ProbParams

# First, we'll integrate over the canonical n simplex  
def f(x):
	return x


