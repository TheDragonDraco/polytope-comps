#!/usr/bin/sage -python
from sage.all import *
from sage.symbolic.integration.integral import definite_integral
from operator import mul
from itertools import product
from scipy.misc import comb
import numpy as np

def binseq(nmin):
	return [''.join(x) for x in product('01', repeat = nmin)]


def fpar(x,s):
	return 2/(s**2) * x
	

def check_pdf(fpar,s):
	x = var('x')
	f = fpar(x,s)
	print 'f = ', f
	assert(definite_integral(fpar(x,s),x,0,s) == 1)	


def get_xarr(n):
	return list(var('x%d' % i) for i in range(1,n+1))

def get_fjoint(fpar,n,s):
	xarr = get_xarr(n)
	fjoint = factorial(n) * reduce(mul,[ fpar(xi,s) for xi in xarr],1)
	return fjoint 	



def get_sarr(n):
	return 	list(var('s%d' % i) for i in range(1,n+1))


def get_fjoint_slacks(fpar_joint,n,s):
	sarr = get_sarr(n)
	sum_sarr = np.cumsum(sarr)
	fjoint = factorial(n) * reduce(mul,[ fpar(si,s) for si in sum_sarr],1)
	return fjoint 	



def integ_Pos(fjoint, xarr,n,s):
	I = fjoint
	for i in xrange(n-1):
		I = definite_integral(I,xarr[i],0,xarr[i+1])
	return definite_integral(I,xarr[n-1], 0,s)



def integ_Slack(fjoint,sarr,n,s):
	I = fjoint

	sarr = get_sarr(n)
	sum_sarr = np.cumsum(sarr)

	
	for i in xrange(n-1,0,-1):
		I = definite_integral(I, sarr[i], 0, s - sum_sarr[i-1])
			
	return definite_integral(I, sarr[0], 0,s)

def integ_Slack_Disconn(fjoint,sarr,disconn, d,n,s):
	I = fjoint

	sarr = get_sarr(n)
	sum_sarr = np.cumsum(sarr)

	assert(len(sarr) == n)
	assert(len(disconn) == n+1)
	total = s - disconn[n] * d
	#print 'Disconn = ', disconn
	#print 'Starting total = ', total
	
	for i in xrange(n-1,0,-1):
		print 'Integrating ', sarr[i], ':', disconn[i] * d, ' ... ', total - sum_sarr[i-1]
		I = definite_integral(I, sarr[i], disconn[i] * d, total - sum_sarr[i-1])
		total = total - disconn[i] * d
	
	print 'Integrating ', sarr[0], ':', disconn[0] * d , total		
	I = definite_integral(I, sarr[0], disconn[0] * d , total)
	return I.full_simplify()



def get_Prob_Slacks(fjoint, sarr,d,n,s,nmin):
	prob = 0
	sign = 1
	for elem in binseq(n + 1):
		
		l = [int(i) for i in list(elem)]
		if (0 < sum(l) <= nmin):			
			I = integ_Slack_Disconn(fjoint,sarr,l,d,n,s)
			parity = sum(l) - 1 
			sign = (-1) ** parity
			#print l, sign * I
			prob = prob + sign * I
			#print 'now, prob = ', prob
	return prob.full_simplify()



def integ_Disconn(fjoint,xarr,disconn,d,n,s):
	I = fjoint
	count = 0
	print xarr, disconn
	assert(len(xarr) == n)
	assert(len(disconn) == n)
	
	xarr2 = xarr[:]
	xarr2.append(s)
	
	#print disconn,
	for i in xrange(n):	
		count += disconn[i]
		lower = count * d
		upper = xarr2[i+1]
		print xarr2[i], lower,"--", upper, "," 		
		I = definite_integral(I,xarr2[i], lower, upper)
		print I

	 
	return I


def get_Prob_Uniform(d,n,s,nmin):
	prob = 0
	sign = 1
	for i in xrange(1, nmin + 1):
		term = (s - i * d  )** n  * comb(n + 1, i)
		print i, sign * term
		prob += sign * term
		sign *= -1

	prob = prob / (s**n)
	return prob.full_simplify() 
		
	

def get_Prob(fjoint, xarr,d,n,s,nmin):
	prob = 1
	sign = 1
	for elem in binseq(n):
		
		l = [int(i) for i in list(elem)]
		if (0 < sum(l) <= nmin ):

			I = integ_Disconn(fjoint,xarr,l,d,n,s)
			print "I = ",I
			sign = (-1) ** (sum(l) - 1)
			prob = prob + sign * I
			
	return prob.full_simplify()



## Main
s = var('s')
d = var('d')

assume(s,'real')
assume(s>0)
assume(d,'real')
assume(d>0)

check_pdf(fpar,s)



n = 2
xarr = get_xarr(n)
print 'xarr=',xarr


sarr = get_sarr(n)

print 'sarr =', sarr
fjoint = get_fjoint(fpar,n,s)
print 'fjoint=',fjoint
I0 = integ_Pos(fjoint,xarr,n,s)
print 'Integral over psimp = ', I0 


fjoint_sla = get_fjoint_slacks(fpar,n,s)
print 'fjoint_sla=', fjoint_sla

I1 = integ_Slack(fjoint_sla,sarr,n,s)
print 'integral over slack simplex = ', I1

d = 8
s = 20.

nmin = int(s/d)
print get_Prob(fjoint, xarr, d,n,s,nmin)










