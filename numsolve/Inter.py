#!/usr/bin/python 
import numpy as np
from scipy.misc import factorial
from itertools import product
from copy import deepcopy
from Hsp import Hsp 
from Cuboid import Cuboid

## Simplex hypercube and related intersections
class Inter:

	def __init__(self, hsp, cuboid):
		assert(cuboid.dim == hsp.dim)
		self.hsp = hsp
		self.cuboid = cuboid
			

	def getDim(self):
		return self.hsp.dim


	def getVol(self):
		avec = self.hsp.avec
		b = self.hsp.b
		n =  self.getDim()

		const = np.prod(avec) * factorial(n)
		vol = 0
		

		for v in self.cuboid.getVerts():
			parity = np.count_nonzero(v) % 2
			print('vertex = ',  v)
			sign = 1
			if (parity == 1):
			    sign = -1
	
		
			res =  self.hsp.getRes(v)			
			if (res > 0):
				 vol += sign * (res ** n)

		return vol / const

	
			
	
					
		

