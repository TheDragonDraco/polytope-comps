#!/usr/bin/sage -python

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from scipy.misc import comb
from ProbParams import ProbParams
from Inter import Inter
from scipy.misc import factorial
from math import sqrt

from AttScen import AttScen
from AttUtils import *
from scipy.special import btdtr
from sage.symbolic.integration.integral import definite_integral
from scipy.stats import beta
from scipy.special import gamma

def constructParams(s,n,dia, d):
	return ProbParams(s,n,dia,d,allowColls = True)


def getBeta(a,b, x):
	return	 gamma(a+b) * x**(a-1) * (1-x)**(b-1) / (gamma(a) * gamma(b))	



if __name__ == "__main__": 
	s= var('s')
	x1 = var('x1')
	x2 = var('x2')
	assume(s>0)
	d = var('d')
	assume(d>0)
	assume(d<s)

	c = 2 / (s**2)
	I1 = definite_integral(definite_integral(c,x2,x1,x1+d), x1,0,s-d)
	I2 = definite_integral(definite_integral(c,x2,x1,s), x1,s-d,s)
	print (I1+I2).full_simplify()

	x = var('x')
	f= getBeta(1,3,x)
	I1 = definite_integral(f,x,0,d/s) 
	I2 = definite_integral(f,x,d/s,1)
	print I1/(I1+I2)
	
	

