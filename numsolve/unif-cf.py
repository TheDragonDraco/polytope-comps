#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from scipy.misc import comb
from ProbParams import ProbParams
from Inter import Inter
from scipy.misc import factorial
from math import sqrt, log

from AttScen import AttScen
from AttUtils import *
from scipy.special import btdtr




def findpmon(params):
	#assert(params.dia == 0)
	[s,n,d] = [params.getFreeSlack(), params.numRob, params.getFreeCon()]
	if (min(s,n,d) <=0 ):
		return -1
	
	nmin = int(s/d)
	pmonbar = 0
	sign = 1
	for i in xrange(1,nmin + 1):
		prob = sign * comb(n+1,i) * (1-i*d/s)**n 
		sign *= -1
		pmonbar += prob

	return 1 - pmonbar


	

def findpsen(params):
	[s,n,d] = [params.getFreeSlack(), params.numRob, params.getFreeCon()]

	if (min(s,n,d) <=0 ):
		return -1
	
	nmin = int(s/d) # be on the safe side
	if (n < nmin):
		return 0
	psenbar = 0
	sign = 1
	if (n <= 1 ):
		return -1
	#print 'nmin = ', nmin
	for i in xrange(1,nmin + 1):
		#print 'i=', i
		# Happens when v1=v_{n+1} = 0, and the remaining comes from v_{2:n}
		sides = [(1-2*i*d/s), 1- ( d* (2*i -1)) /s , 1 - 2*(i+1)*d/s]
		#print 'sides = ', sides
		weights = [comb(n-1,i), 2 * comb(n-1,i-1), comb(n-1,i-2)] # short circuits to zero if 	
		#print 'weights=', weights		
		probs = [0,0,0]
		for j in xrange(3):
			if (sides[j] > 0):
				probs[j] = weights[j] * (sides[j] ** n)
			 
		
		#print 'probs =', probs,'sumprob =', sum(probs)
				
		#print 'side of pmon=', 1 - i*(d/s), i * (d/s)
		#print 'pmonprob =', comb(n+1,i) * max(1-i*d/s,0) **n
		prob = sum(probs) * sign
		psenbar += prob
		sign *= -1

	return 1 - psenbar

	


def findpcon(params):
	[s,n,d] = [params.getFreeSlack(), params.numRob, params.getFreeCon()]
	if (min(s,n,d) <=0 ):
		return -1

	

	nmin = int(s/d)
	lim = min(nmin, n - 1)	
	pcon = 0
	sign = 1
	for i in xrange(lim + 1):
		term = sign * comb(n - 1 , i) * (1 - i * d / s) **n 
		sign *= -1
		pcon += term

	return pcon


def findexpcmp(params):
	[s,n,d] = [params.getFreeSlack(), params.numRob, params.getFreeCon()]
	if (min(s,n,d) <=0 ):
		return 200
	

	nmin = int(s/d)
	return 1 + (n-1) * (1-d/s)**n	

def findexpdeg(params):
	[s,n,d] = [params.getFreeSlack(), params.numRob, params.getFreeCon()]

	if (min(s,n,d) <=0 ):
		return 10000

	sf = s
	df = d
	if (min(sf,n,df) <=0 ):
		return 1000

	return (n-1) * (2 * df * sf - df**2) /(sf**2)	
	



if __name__ == "__main__": 
	pmon = 0.80
 	s = 200.0
 	d = 5.0; dia = 1	
	

 	### PMON

 	print 'solving for pmon =', pmon
 	findpmon_n = lambda n :findpmon(ProbParams({'totalSlack':s,'numRob': n,'dia':dia,'satDist':d, 'allowColls':False})) - pmon
 	findpmon_d = lambda d: findpmon(ProbParams({'totalSlack':s,'numRob': n,'dia':dia,'satDist':d, 'allowColls':False})) - pmon
 
	

	#logfunc = lambda n: log(n+1)/(n+1) - d / (s * pmon)
	params =  ProbParams({'totalSlack':s,'numRob': 1,'dia':dia,'satDist':d, 'allowColls':False})
	logfunc = lambda n: log(n+1)/(n+1) - d / (s * pmon)
	n_solved_est = fsolve(logfunc, 12.00, xtol = 0.001)
	print 'initial guess =', n_solved_est, logfunc(n_solved_est) - d / (s * pmon)

	n_solved = fsolve(findpmon_n, n_solved_est, xtol = 0.001)
 	print 'n:',n_solved[0], '\terror:',findpmon_n(int(n_solved[0])), '\tpmon at n:', findpmon(ProbParams({'totalSlack':s,'numRob': int(n_solved[0]),'dia':dia,'satDist':d, 'allowColls':False}))


	### PCON
	pcon = 0.7	
	print '\nsolving for pcon = ', pcon
	
	findpcon_n = lambda n :findpcon(ProbParams({'totalSlack':s,'numRob': n,'dia':dia,'satDist':d, 'allowColls':False})) - pcon
	n_solved = fsolve(findpcon_n, n_solved_est, xtol = 0.001)
	print 'n:', n_solved[0], '\tError:', findpcon_n(int(n_solved[0])), '\tpcon at n:', findpcon(ProbParams({'totalSlack':s,'numRob': int(n_solved[0]),'dia':dia,'satDist':d, 'allowColls':False}))   	

	## EXPCMP
	expcmp = 4
	print '\nsolving for cmp =', expcmp 
	findexpcmp_n = lambda n :findexpcmp(ProbParams({'totalSlack':s,'numRob': n,'dia':dia,'satDist':d, 'allowColls':False}))  - expcmp	
	n_solved = fsolve(findexpcmp_n, int(n_solved_est), xtol = 0.001)
	print 'n:',n_solved[0], '\nError:',findexpcmp_n(int(n_solved[0])), '\texpcmp at n:',findexpcmp(ProbParams({'totalSlack':s,'numRob': int(n_solved[0]),'dia':dia,'satDist':d, 'allowColls':False}))

	

	#### SLEN	
	expslen = 0.6 * s
	psen = expslen / s
	'''for n in xrange(45,200):
		params = ProbParams(s,n,0,d)	
		print 'n=', n 
		psen =findpsen(params)
		print 'psen=',psen,'\t comparing with ', findpmon(params), findpmon(ProbParams(s,n,0,2*d))
	'''	
	print '\nsolving for pslen =', psen 
	findpsen_n = lambda n :findpsen(ProbParams({'totalSlack':s,'numRob': n,'dia':dia,'satDist':d, 'allowColls':False})) - psen
	n_solved = fsolve(findpsen_n, int(n_solved_est), xtol = 0.001)	
	print 'n:',n_solved[0], '\terror:',findpsen_n(int(n_solved[0])), '\tpsen at n:',findpsen(ProbParams({'totalSlack':s,'numRob': int(n_solved[0]),'dia':dia,'satDist':d, 'allowColls':False}))	
	

	## EDEG
	edeg = 5
	print '\n solving for expected degree=', edeg
	findexpdeg_n = lambda n :findexpdeg(ProbParams({'totalSlack':s, 'numRob':n, 'dia':dia, 'satDist':d, 'allowColls':False})) - edeg
	n_solved = fsolve(findexpdeg_n, int(n_solved_est[0]), xtol = 0.001)	
	print 'n:', n_solved , '\texpcmp at n:',findexpdeg(ProbParams({'totalSlack':s,'numRob':int(n_solved),'dia':dia,'satDist':d,'allowColls':False}))


