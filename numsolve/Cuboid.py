#!/usr/bin/python 
import numpy as np
from scipy.misc import factorial
from itertools import product
from copy import deepcopy


class Cuboid:


	def __init__(self,sides):
		#print sides
		assert(np.min(sides) > 0)
		self.sides = deepcopy(sides)
		#print self.sides	
		self.dim = sides.shape[0]
		

	def getVol(self):
		return np.prod(self.sides)


	def getVerts(self):
		verts = []
		for v in product([0,1], repeat = self.dim):
			p = v * self.sides
			verts.append(p)				
			
		return verts



		
	
