#!/usr/bin/python 
import numpy as np
from scipy.misc import factorial
from itertools import product
from copy import deepcopy


class Hsp:


	def __init__(self,avec,b):
		#print sides
		assert(np.min(avec) > 0)
		self.avec = deepcopy(avec)
		#print self.sides	
		assert(b > 0)
		self.b = b
		self.dim = avec.shape[0]
		self.getIntercepts()
		
	def getDim(self):
		return self.dim		


	def getIntercepts(self):
		self.inter = np.array([])
		for a in self.avec:
			self.inter = np.append(self.inter,self.b/a)

	# get residue
	def getRes(self,p):
		assert(p.shape[0] == self.dim)
		return self.b - np.dot(self.avec,p) 

	def getVol(self):
		return np.prod(self.inter) / factorial(self.dim)
