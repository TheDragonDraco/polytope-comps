#!/usr/bin/sage -python
from sage.all import *
from sage.symbolic.integration.integral import definite_integral
from operator import mul
from itertools import product
from scipy.misc import comb
import numpy as np
from ProbParams import ProbParams
from sympy import sqrt

def pdf(x,s):	
	return 2 * x / s**2  

def pdfInv(y,s):
	return y * (s ** 2) / 2

def cdf(x,s):
	return x**2 / s ** 2

def cdfInv(y,s):
	return sqrt(y * s ** 2)


# First, we'll integrate over the canonical n simplex  

class Parent:
	
	def __init__(self,params, pardict):
		self.pardict = pardict	
		self.parpdf = self.pardict['pdf']
		self.params = params

		# Initialize xarr
		n = self.params.numRob
		self.xarr = list(var('x%d' % i) for i in range(1,n+1))

		s = params.totalSlack
		
		self.jfx = factorial(n) * reduce(mul,[ self.parpdf(xi,s) for xi in self.xarr],1) 
		
		# Initialize sarr
		self.sarr = list(var('s%d' % i) for i in range(1,n+1)) # Skipping the last slack
		self.sum_sarr = np.cumsum(self.sarr)
		s = self.params.totalSlack
		self.jfs = factorial(n) * reduce(mul,[ self.parpdf(si,s) for si in self.sum_sarr],1)
	
	def getOsDict(self,r):

		assert (r >= 1)
		
		n = self.params.numRob
		assert (r <= n)		
		F = self.pardict['cdf']
		self.osDict = {'r:', r ,  'cdf': 0, 'pdf': None}
		x = var('x')	
		s = self.params.totalSlack

		for k in xrange(r, n +1):
			self.osDict['cdf'] += comb(n,k) * (cdf (x,s)  ** k ) + (1.0 - cdf(x,s)) ** (n - k)
		self.osDict['pdf'] = derivative(self.osDict['cdf'],x)

		# We'll get the expectation 
		x = var('x')
		assume(x, 'real')
		

			

		

				
if __name__ == "__main__":

	x = var('x')

	s = var('s')
	n = var('n')
	dia = 0
	d = var('d')
	
	params = ProbParams(s,10 , dia,d)
	print params.totalSlack
	fdict = {'pdf': pdf, 'pdfInv': pdfInv, 'cdf': cdf, 'cdfInv': cdfInv}
	P = Parent( params, fdict)

	print 'Joint pos = ', P.jfx
	print 'Joint sla = ', P.jfs

	#Integrating : over slack simplex
	print 'Integrating slacks'
	mvf = P.jfs
	
	s = params.totalSlack
	# We'll start integrating
	slimits = [ [0, s] ]
	for xi in P.sum_sarr:
		slimits.append([0, s - xi])
	print slimits

	P.getOsDict(4)
		

	# Now do the integrals backwards
	
	


	


